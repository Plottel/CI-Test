﻿using System;
using SwinGameSDK;

namespace MyGame
{
	public class Line : Shape
	{
		private float _endX, _endY;

		public Line() : this(Color.Red, 0, 0, 0, 0)
		{
		}

		public Line(Color color, float startX, float startY, float endX, float endY) : base(color)
		{
			X = startX;
			Y = startY;
			_endX = endX;
			_endY = endY;
		}

		public float EndX
		{
			get
			{
				return _endX;
			}
			set
			{
				_endX = value;
			}
		}

		public float EndY
		{
			get
			{
				return _endY;
			}
			set
			{
				_endY = value;
			}
		}

		public override void Draw()
		{
			if (Selected)
			{
				DrawOutline();
			}
			SwinGame.DrawLine(Color, X, Y, _endX, _endY);
		}

		public override void DrawOutline()
		{
			SwinGame.DrawCircle(Color.Black, X, Y, 10);
			SwinGame.DrawCircle(Color.Black, _endX, _endY, 10);
		}

		public override bool IsAt(Point2D pt)
		{
			return SwinGame.PointOnLine(pt, X, Y, _endX, _endY);
		}
	}
}