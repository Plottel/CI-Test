﻿using NUnit.Framework;
using System;
using SwinGameSDK;
using MyGame;
//Add using your program's namespace here.
//e.g. if the namespace of your program is called "SwinAdventure" you would type...
//using SwinAdventure;

namespace UnitTests
{
    [TestFixture]
    public class MyUnitTests
    {
        MyGame.Circle myShape;
        //Declare variables here. DON'T give variables a value here

        [SetUp]
        public void Init()
        {
            myShape = new MyGame.Circle(Color.Green, 50);
        }

        [Test]
        public void TestCircleRadius()
        {
            Assert.AreEqual(50, myShape.Radius);
        }
    }
}