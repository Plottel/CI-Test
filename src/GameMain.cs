using System;
using System.Collections.Generic;
using SwinGameSDK;

namespace MyGame
{
    public class GameMain
    {
		private enum ShapeKind
		{
			Rectangle,
			Circle,
			Line
		}

        public static void Main()
        {
			Drawing myDrawing = new Drawing();
			ShapeKind kindToAdd = ShapeKind.Circle;

            //Open the game window
            SwinGame.OpenGraphicsWindow("ShapeDrawing", 800, 600);
            
            //Run the game loop
            while(false == SwinGame.WindowCloseRequested())
            {
                //Fetch the next batch of UI interaction
                SwinGame.ProcessEvents();
                
                //Clear the screen
                SwinGame.ClearScreen(Color.White);

				if (SwinGame.MouseClicked(MouseButton.LeftButton))
				{
					Shape newShape;

					if (kindToAdd == ShapeKind.Circle)
					{
						newShape = new Circle();
					}
					else if (kindToAdd == ShapeKind.Rectangle)
					{
						newShape = new Rectangle();
					}
					else
					{
						newShape = new Line();
						(newShape as Line).EndX = SwinGame.MouseX() + 50;
						(newShape as Line).EndY = SwinGame.MouseY() + 50;
					}

					newShape.X = SwinGame.MouseX();
					newShape.Y = SwinGame.MouseY();

					myDrawing.AddShape(newShape);
				}

				if (SwinGame.MouseClicked(MouseButton.RightButton))
				{
					myDrawing.SelectShapesAt(SwinGame.PointAt(SwinGame.MouseX(), SwinGame.MouseY()));
				}

				if (SwinGame.KeyTyped(KeyCode.RKey))
				{
					kindToAdd = ShapeKind.Rectangle;
				}

				if (SwinGame.KeyTyped(KeyCode.CKey))
				{
					kindToAdd = ShapeKind.Circle;
				}

				if (SwinGame.KeyTyped(KeyCode.LKey))
				{
					kindToAdd = ShapeKind.Line;
				}

				if (SwinGame.KeyTyped(KeyCode.DeleteKey) || SwinGame.KeyTyped(KeyCode.BackspaceKey))
				{
					foreach (Shape s in myDrawing.SelectedShapes)
					{
						myDrawing.RemoveShape(s);
					}
				}

				if (SwinGame.KeyTyped(KeyCode.SpaceKey))
				{
					myDrawing.Background = SwinGame.RandomRGBColor(255);
				}

				myDrawing.Draw();					
               
                //Draw onto the screen
                SwinGame.RefreshScreen(60);
            }
        }
    }
}
